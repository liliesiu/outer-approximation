#include <boost/filesystem.hpp>
#include <fstream>
#include <iostream>

#include "DerivativeTable.hxx"

int main() {
  boost::multiprecision::mpf_float::default_precision(310);

  std::string filename("data/zzbDerivTable-d3-delta12-0-delta34-0-L0-nmax10-"
                       "keptPoleOrder20-order100.m");

  DerivativeTable dertable(filename);

  auto poly = dertable.derivative(0, 1);

  std::cout << poly << std::endl;

  return 0;
}