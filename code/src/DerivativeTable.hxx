#pragma once

#include <boost/filesystem.hpp>
#include <list>
#include <map>

#include "Bigfloat.hxx"
#include "Polynomial.hxx"

struct DerivativeTable {
  std::vector<std::vector<Polynomial<Bigfloat>>> derivatives;
  size_t Lambda;
  DerivativeTable(const boost::filesystem::path &);
  Polynomial<Bigfloat> &derivative(size_t m, size_t n);
};